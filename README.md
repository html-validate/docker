# html-validate docker images

## Chrome (`htmlvalidate/chrome:latest`)

Debian with Chromium and chromedriver.

With protractor:

    protractor --capabilities.browserName chrome --chromeDriver=/usr/bin/chromedriver

## Firefox (`htmlvalidate/firefox:latest`)

Debian with Firefox and geckodriver.

With protractor:

    protractor --capabilities.browserName firefox --geckoDriver=/usr/bin/geckodriver
